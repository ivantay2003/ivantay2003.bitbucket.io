/*
Copyright Ivan 2020. All rights reserved.
*/


var GENERATE_NRIC = 0
var VALIDATE_NRIC = 1


function scrollToTop() { 
    window.scrollTo(0, 0); 
} 

function nextLine(){
    
    
    var mybr = document.createElement('br');

    
    var p = document.createElement ('p')
    p.appendChild (mybr)
    
    return p
}


function createValidatePage(){

    removePageContainer();
    clearAnswer();
    var divInput = document.createElement ("div")
    divInput.id = "divInput"

    var title =changeTitleUI(VALIDATE_NRIC)
    var pagediv = containerpage();
    // var answer=createAnswerUI()
    var inputNRIC = validateInputUI ();
    var button = validateButtonUI()

    divInput.appendChild(inputNRIC)
    divInput.appendChild(button)

    pagediv.appendChild(title)
    pagediv.appendChild(nextLine())
    // pagediv.appendChild(answer)
    pagediv.appendChild(nextLine())
    // pagediv.appendChild(inputNRIC)
    // pagediv.appendChild(button)
    pagediv.appendChild(divInput)
    document.body.append(pagediv)

    //Disclaimer
    pagediv.appendChild(nextLine())
    pagediv.appendChild(disclaimerUI(VALIDATE_NRIC))
    document.body.append(pagediv)
    
    addEventListenerValidate()
}




function createGenerateNRICPage (){


    var divInput = document.createElement ("div")
    divInput.id = "divInput"
    var divCopyInput = document.createElement("div")
    divCopyInput.id = "divCopyInput"

    removePageContainer();
    clearAnswer();
    var title = changeTitleUI(GENERATE_NRIC)
    var pagediv = containerpage();
    // var answer=createAnswerUI()
    var selectprefix = prefixUI ();
    var selectage = ageUI()
    var button = submitUI();
    var img = barcodeUI();

    divInput.appendChild(selectprefix)
    divInput.appendChild(selectage)
    divInput.appendChild(button)
    // divInput.appendChild(copyUI())
    divCopyInput.appendChild(copyUI())
    pagediv.appendChild(title)
    pagediv.appendChild(nextLine())
    // pagediv.appendChild(answer)
    pagediv.appendChild(nextLine())
    // pagediv.appendChild(selectprefix)
    // pagediv.appendChild(selectage)
    // pagediv.appendChild (button)
    pagediv.appendChild(divInput)
    pagediv.appendChild(divCopyInput)
    pagediv.appendChild(nextLine())
    //pagediv.appendChild(copyUI())
    
    pagediv.appendChild(nextLine())
    pagediv.appendChild(img)
    

    //Disclaimer
    pagediv.appendChild(nextLine())
    pagediv.appendChild(disclaimerUI(GENERATE_NRIC))
    document.body.append(pagediv)

    addEventListenerGenerate()

    
}



function buttonDisable (buttonid){

    var button = document.getElementById(buttonid)
    button.classList.add("disable-button")
    button.classList.remove("enable-button")

}

function buttonEnable (buttonid){

    var button = document.getElementById(buttonid)
    button.classList.add("enable-button")
    button.classList.remove("disable-button")

}

function createButton (text , id){

  
    var nav = document.createElement("nav")
    var ul = document.createElement ("ul")
    var li = document.createElement ("li")
    li.id= id
    li.innerHTML=text
    li.classList.add("button")
    li.classList.add("enable-button")

    var span1= document.createElement("span")
    var span2= document.createElement("span")
    var span3= document.createElement("span")
    var span4= document.createElement("span")
    var span5= document.createElement("span")
    var span6= document.createElement("span")
    

    li.appendChild(span1)
    li.appendChild(span2)
    li.appendChild(span3)
    li.appendChild(span4)
    li.appendChild(span5)
    li.appendChild(span6)
    ul.appendChild(li)
    nav.appendChild(ul)

  
    return nav

}

function changeTitleUI (type){

    var title = document.createElement ("a")
    title.id = "a-title"

    // var title= document.getElementById ("titlesub")


    
    if (type==GENERATE_NRIC)
        title.innerHTML = "Random NRIC Number"
    else if (type == VALIDATE_NRIC)
        title.innerHTML ="Validate NRIC"

    return title;

}



function validateInputUI(){

    var inputtext = document.createElement("input")
    inputtext.id = "input-nric"
    inputtext.name = "nric"
    inputtext.value = "S1234567A"
    //inputtext.classList.add = classname
    inputtext.setAttribute("class" , "input-nric")
    inputtext.setAttribute("size", 10)
    
    return inputtext
}

function validateButtonUI(){

    // var button = document.createElement ("button")
    // button.id = "submit"
    // button.innerHTML = "Validate"

    // return button

    return (createButton( "Validate" , "submit"))
}

function clearAnswer (){

    var answerElement = document.getElementById("nricanswer")
    answerElement.text = ""
}

function ageUI (){

  
    var selectage = document.createElement("select")
    selectage.id="select-age"
    selectage.classList.add ("select-age")

    var data = ["1930-1940","1940-1950","1950-1960","1960-1970","1970-1980", "1980-1990", "1990-2000", "2010-2020", "2010-2020"]

    for (var i=0; i<data.length;i ++){

        var option = document.createElement("option")
        option.id = "age-" + i
        option.classList.add ("option-age", "optionui")
        option.value= (i + 3) ;
        option.innerHTML = data[i]

        selectage.appendChild (option)
    }
    
    return selectage
}

function submitUI (){

    // var button = document.createElement("button")
    // button.id = "btn-submit"
    // button.classList.add ("button" , "btn-submit")
    // button.innerHTML = "Generate NRIC"
    // return button;

    return createButton("Generate NRIC" , "btn-submit")
}

function copyUI (){

    // var button = document.createElement("button")
    // button.id = "btn-copy"
    // button.classList.add ("button" , "btn-copy")
    // button.innerHTML = "Copy NRIC Number"
    // // button.disabled=true
    // return button;

    return createButton("Copy NRIC Number" ,  "btn-copy")

}

function prefixUI(){

    var selectprefix = document.createElement("select")
    selectprefix.id = "select-prefix"

    var data = ["S", "T", "F", "G"]

    for (var i =0; i< data.length; i++){

        var option = document.createElement("option")
        option.id="prefix-" + i
        option.classList.add ("option-prefix", "optionui")
        option.value = data[i]
        option.innerHTML = data[i]

        selectprefix.appendChild (option)

    }
        return selectprefix;

}

function barcodeUI (number){

    if (number==null || number==undefined)
        number= "S2938439H"
    
    var url = "http://barcodes4.me/barcode/c39/" + number + ".png"
    //url = "http://barcodes4.me/barcode/c39/S2938439H.png"

    var imageElement = document.createElement("img")

    imageElement.id = "img-barcode"
    imageElement.setAttribute("alt" , "barcode")
    imageElement.src = "http://barcodes4.me/barcode/c39/S2938439H.png"

    $("#img-barcode").attr("src", url)

    return imageElement;
}

function containerpage (){

    var page = document.createElement ("div")
    page.id = "div-pagecontainer"

    return page
}

function addEventListenerValidate(){

    var button = document.getElementById("submit")
    var input = document.getElementById("input-nric")
    var linkbutton = document.getElementById("btn-link")

    button.addEventListener('click', function(e){

        var inputtext = document.getElementById("input-nric").value

        var resultboolean = icValidate (inputtext.toUpperCase())


        if (resultboolean){
            document.getElementById('nricanswer').text = "Valid NRIC Number"

        }
        else{
            document.getElementById('nricanswer').text = "Invalid NRIC Number"

        }

        scrollToTop();

    })

    input.addEventListener("click", function(e){


        $(document).ready(function() {
            $("#input-nric").select().focus(function(e) { $(this).select(); } );
        });
    })

    input.addEventListener("mouseenter", function(e){


        $(document).ready(function() {
            $("#input-nric").select().focus(function(e) { $(this).select(); } );
        });
    })

    linkbutton.addEventListener ('click',function(e){

        e.preventDefault()
        createGenerateNRICPage()

    })

}

function addEventListenerGenerate (){

    var ageUI = document.getElementById ("select-age")
    var prefixUI = document.getElementById("select-prefix")
    var buttonUI = document.getElementById("btn-submit")
    var copyUI = document.getElementById("btn-copy")
    var linkbutton = document.getElementById("btn-link")

    //var age;
    //var agevalue;
   
    buttonDisable("btn-copy")

    ageUI.addEventListener('change', function(e){

        e.preventDefault()

        var agevalue = ageUI.options[ageUI.selectedIndex].value


        if (agevalue>=10)
            prefixUI.value = "T";
        else if (agevalue<=10)
            if (prefixUI.value =="T")
                prefixUI.value="S"


        
    })

    prefixUI.addEventListener('change', function (e){
        e.preventDefault()
        var prefixvalue = prefixUI.options[prefixUI.selectedIndex].value

        if ((prefixvalue=="T") || (prefixvalue=="G"))
            ageUI.value="10"
        else if ((prefixvalue=="S") || (prefixvalue=="F"))
            ageUI.value="9"
    })

    buttonUI.addEventListener('click', function(e){

        e.preventDefault()


        age = ageUI.options[ageUI.selectedIndex].value
        prefix = prefixUI.options[prefixUI.selectedIndex].value


        var nric = generateNRIC()

        document.getElementById('nricanswer').text = nric
        var img = barcodeUI(nric)
        // copyUI.disabled=false;

        buttonEnable("btn-copy")
        
        scrollToTop();

    })

    copyUI.addEventListener('click', function(e){
     
        e.preventDefault()

        var answer= document.getElementById("nricanswer")


        copyElementText("nricanswer")


        

    })

    linkbutton.addEventListener ('click',function(e){

        e.preventDefault()
        createValidatePage()

    })
}

function copyElementText(id) {
    var text = document.getElementById(id).innerHTML;
    var elem = document.createElement("textarea");
    document.body.appendChild(elem);

    elem.value = text;
    elem.select();
    document.execCommand("copy");
    document.body.removeChild(elem);
}

function generateNRIC (){
    var selectage = document.getElementById ("select-age")
    agevalue = parseInt(selectage.options[selectage.selectedIndex].value, 10) 

    agevalue = parseInt(agevalue)
    if (agevalue>=10)
    agevalue=agevalue-10

    var selectprefix = document.getElementById("select-prefix")
    prefixvalue = selectprefix.options[selectprefix.selectedIndex].value

    return generateNumber (prefixvalue,agevalue)
    

}

function commaUIa (){

    var creditcomma = document.createElement("a")
    creditcomma.id = "a-creditcomma"
    creditcomma.innerHTML = "     ,     "
    creditcomma.classList.add("a-comma")

    return creditcomma;
}


function disclaimerUI (type){

    var disclaimerdiv = document.createElement ("div")
    disclaimerdiv.id = "div-disclaimer"


    var linkpage = document.createElement("a")
    linkpage.id="linkpage"
    var button;
    if (type==VALIDATE_NRIC){

        linkpage.innerHTML = "Generate NRIC Number Page"
        //linkpage.setAttribute("onClick", "createGenerateNRICPage()")
        //linkpage.href="index.html"
        button = createButton("Generate Number", "btn-link")

    }else if (type==GENERATE_NRIC){

        linkpage.innerHTML = "Validate NRIC Number Page"
        //linkpage.setAttribute("onClick", "createValidatePage()")
        //linkpage.href="index.html"
        button = createButton("Validate Number", "btn-link")
    }

    var disclaimerIntro = document.createElement ("p")
    disclaimerIntro.id = "p-intro"
    disclaimerIntro.innerHTML = "This site is used to study how a NRIC number is generated."
    
    var disclaimerIntro1 = document.createElement ("p")
    disclaimerIntro1.id = "p-intro1"
    disclaimerIntro1.innerHTML = "This site is created solely for educational purposes."

    var disclaimerP = document.createElement ("p")
    disclaimerP.id = "p-disclaimer"
    disclaimerP.innerHTML = "By generating or/and copying NRIC or FIN number from this site,"
    disclaimerP.classList.add ("disclaimer")

    var disclaimerP1 = document.createElement ("p")
    disclaimerP1.id = "p-disclaimer1"
    disclaimerP1.innerHTML = "you hereby agree to be responsible for your actions for use of the numbers,"
    disclaimerP1.classList.add ("disclaimer")

    var disclaimerP2 = document.createElement ("p")
    disclaimerP2.id = "p-disclaimer2"
    disclaimerP2.innerHTML = "and waive all your rights to hold me liable to any problems arising from your actions."
    disclaimerP2.classList.add ("disclaimer")

    var disclaimerP3 = document.createElement ("p")
    disclaimerP3.id = "p-disclaimer3"
    disclaimerP3.innerHTML = "You agree not use this site for ANY illegal activities, and not to use services from this site on government and business entities ."
    disclaimerP3.classList.add ("disclaimer")

    var disclaimerP4 = document.createElement ("p")
    disclaimerP4.id = "p-disclaimer4"
    disclaimerP4.innerHTML = "By using this online tool, you agree to indemnify Ivan Tay from any legal implications or loss of income or any damanges."
    disclaimerP4.classList.add ("disclaimer")

    var disclaimerP10 = document.createElement("p")
    disclaimerP10.id = "p-disclaimer10"
    disclaimerP10.innerHTML = "The algorithm is open source in the public domain. Please refer to the link below"
    disclaimerP10.classList.add ("algo-open")

    var disclaimerRisk1 = document.createElement("p")
    disclaimerRisk1.id = "p-disclaimer10"
    disclaimerRisk1.innerHTML = "Use at your own risk"
    disclaimerRisk1.classList.add ("disclaimer-risk")
    
    var howalgocreate = document.createElement("a")
    howalgocreate.id = "a-howalgo"
    howalgocreate.innerHTML = "How NRIC number is generated"
    howalgocreate.href = "https://medium.com/@ivantay2003/creation-of-singapore-identity-number-nric-24fc3b446145"
    howalgocreate.classList.add("credits-others")

    var copyright = document.createElement("p")
    copyright.id = "p-copyright"
    copyright.innerHTML = "The page is created by "
    copyright.classList.add ("author-link")

    var copyrightHyper = document.createElement ("a")
    copyrightHyper.id = "a-copyright"
    copyrightHyper.innerHTML ="Ivan Tay"
    copyrightHyper.href ="http://linkedin.com/in/ivantay"
    copyrightHyper.classList.add ("author-link")

    copyright.appendChild(copyrightHyper)

    var ul = document.createElement("ul")
    ul.id = "ul-credits"

    var creditli = document.createElement("li")
    creditli.id = "li-credit1"
    creditli.innerHTML = "Credit source of research from "



    var credit1a = document.createElement("a")
    credit1a.id = "a-credit1a"
    credit1a.innerHTML = "Samuel Liew "
    credit1a.href = "https://samliew.com/singapore-nric-validator"


       

    var creditspace1 = document.createElement("a")
    creditspace1.id = "a-creditspace1"
    creditspace1.innerHTML = "     and     "

    var credit1b = document.createElement("a")
    credit1b.id = "a-credit1b"
    credit1b.innerHTML = "Wikepedia "
    credit1b.href = "https://en.wikipedia.org/wiki/National_Registration_Identity_Card#Structure_of_the_NRIC_number.2FFIN"



    
    var credit1c = document.createElement("a")
    credit1c.id = "a-credit1b"
    credit1c.innerHTML = "Ayumlive blog "
    credit1c.href = "https://ayumilovemaple.wordpress.com/2008/09/24/validation-singapore-nric-number-verification/"

    var credit1d = document.createElement("a")
    credit1d.id = "a-credit1d"
    credit1d.innerHTML = "Eddie Moore "
    credit1d.href = "https://gist.github.com/eddiemoore/7131781"


    disclaimerdiv.appendChild(button)
    
    disclaimerdiv.appendChild(linkpage)
    disclaimerdiv.appendChild(nextLine())
    disclaimerdiv.appendChild(disclaimerIntro)
    disclaimerdiv.appendChild(disclaimerIntro1)
    disclaimerdiv.appendChild(nextLine())
    disclaimerdiv.appendChild (disclaimerP)
    disclaimerdiv.appendChild (disclaimerP1)
    disclaimerdiv.appendChild (disclaimerP2)
    disclaimerdiv.appendChild(nextLine())
    disclaimerdiv.appendChild (disclaimerP3)
    disclaimerdiv.appendChild (disclaimerP4)
    disclaimerdiv.appendChild(nextLine())
    disclaimerdiv.appendChild(disclaimerRisk1)
    disclaimerdiv.appendChild(nextLine())
    disclaimerdiv.appendChild(nextLine())
    disclaimerdiv.appendChild (disclaimerP10)
    disclaimerdiv.appendChild(howalgocreate)
    disclaimerdiv.appendChild(nextLine())
    disclaimerdiv.appendChild(nextLine())
    disclaimerdiv.appendChild(copyright)

    creditli.appendChild(credit1a)
    creditli.appendChild(commaUIa())
    creditli.appendChild(credit1b)
    creditli.appendChild(commaUIa())
    creditli.appendChild(credit1c)
    creditli.appendChild(creditspace1)
    creditli.appendChild(credit1d)
    ul.appendChild (creditli)
    disclaimerdiv.appendChild(ul)
    //disclaimerdiv.appendChild(credit1a)
    

    

    return disclaimerdiv
}

function refresh(){


    createGenerateNRICPage()
    //createValidatePage();



}

function removePageContainer(){

    if ($('#div-pagecontainer').length)
        $('#div-pagecontainer').remove()    

}


function refreshElement (element){

    $(document).read(function(){

        
    })
}

refresh();