/*
Copyright Ivan 2020. All rights reserved.
*/


function pad(num, size) {
    var temp = "0000000" + num;
    return temp.substr(temp.length-size);
  }
  
  

function icValidate (str){



    let maxArray = 9
    var nricarray = new Array (8);
    var weight =0;
    var offset=0;

    for (var i  =0 ; i< maxArray; i++){

        nricarray [i]= str.charAt(i)


    }

    nricarray[1] *= 2;
    nricarray[2] *= 7;
    nricarray[3] *= 6;
    nricarray[4] *= 5;
    nricarray[5] *= 4;
    nricarray[6] *= 3;
    nricarray[7] *= 2;

    for (var i = 1; i < 8; i++) {
        weight += parseInt(nricarray[i]);
      }


    var offset = (nricarray[0] == "T" || nricarray[0] == "G") ? 4 : 0;

    if (nricarray[0]== "G" || nricarray[0]=="T" )
      offset = 4
    else
      offset = 0;
    
    var remaindermod = (weight+offset) %11;

    var foreigner = Array("X", "W", "U", "T", "R", "Q", "P", "N", "M", "L", "K");
    var singaporean = Array("J", "Z", "I", "H", "G", "F", "E", "D", "C", "B", "A");

    var alpha;
    
    if (nricarray[0] == "F" || nricarray[0] == "G") 
        alpha = foreigner[remaindermod];
    else if (nricarray[0] == "S" || nricarray[0] == "T") 
        alpha = singaporean[remaindermod];

    return (nricarray[8] == alpha);
}


function icGenerate (prefix, age){

    var total=0 , offset=0,  alpha="";

    age = parseInt (age, 10)

    //Validate
    if (!(age >= -1 && age <= 9)) 
        age = -1;

    if (prefix != 'S' && prefix != 'T' && prefix != 'F' && prefix != 'G') 
        return null;

    //NRIC generator algorithm
    var nricarray = pad(Math.floor(Math.random() * 9999999), 7).split('');
    if (age != -1) nricarray[0] = age;

    var number_output = prefix + nricarray.join('');
    
    nricarray[0] *= 2;
    nricarray[1] *= 7;
    nricarray[2] *= 6;
    nricarray[3] *= 5;
    nricarray[4] *= 4;
    nricarray[5] *= 3;
    nricarray[6] *= 2;

    

    for (var i = 0; i <= 6; i++) {
        total += nricarray[i];
      }
    
    if (prefix=="G" || prefix=="T")
        offset=4
    else
        offset=0  
      
    var remaindermod = (total + offset) % 11;

    var foreigner = Array("X", "W", "U", "T", "R", "Q", "P", "N", "M", "L", "K");
    var singaporean = Array("J", "Z", "I", "H", "G", "F", "E", "D", "C", "B", "A");

    if (prefix == "T" || prefix == "S") 
        alpha = singaporean[remaindermod]; 

    else if (prefix=="G" || prefix== "F")
        alpha = foreigner[remaindermod];
    
    else 
      alpha = null

    //   console.log (alpha)
    if (alpha==null)
        return null
    else
      return number_output + alpha
    

}


function generateNumber (prefix, age){


    raw_num = icGenerate(prefix,age)
  
    endyear = age
    return replaceChar(raw_num, endyear, 1)
    //return raw_num.replaceAt(0,age)
  
    // console.log (raw_num)
  
  }


function replaceChar(origString, replaceChar, index) 
{ 
    let firstPart = origString.substr(0, index); 
      
    let lastPart = origString.substr(index + 1); 

    let newString =  
        firstPart + replaceChar + lastPart; 
      
    return newString; 
} 

// str="S7696960D"
// console.log (icValidate (str))

// console.log (icGenerate("S", 40))
