//function setimage (text){
//    
//    
//   var src = setQR(text) 
//   var img = document.getElementById("image")
//   img.src = src
//    console.log ("field : " + text)
//    var textinput = document.getElementById("answer")
//    textinput.value = text
//    //window.location.reload()
//}

//https://www.online-qrcode-generator.com/

let MAX_TEXT = 80
let MAX_WIDTH = 5
let MAX_PHONE_TEXT = 15

let typePhone = "PHONE"
let typeURL = "URL"
let typeFREE = "FREE"
let typeSMS = "SMS"
let typeWIFI = "WIFI"
let typeEMAIL = "EMAIL"

function setimage (text){
    
    
   var src = setQR(text) 
   
   var container = document.getElementById("page-container")
   var img = document.getElementById("image")
   img.src = src
    
    container.appendChild(img)
    
    var textinput = document.getElementById("answer")
    textinput.value = text
    //window.location.reload()
    
   // creditelement()
}



//function selectPage (paramater ){
//    
//    alert ("clickme : " + paramater)
//    
//    
//    
//    if (paramater=="free")
//        pageFreeText
//    else if (paramater=="url")
//        pageURL()
//}


function pageEmail () {
    
    pageOneLinerText ("Type Any Email", "john@example.com", "Your Email", typeEMAIL)
}
function pageWIFI (){
    
        pageTwoLinerText ("WIFI ID", "Password" , typeWIFI)
}

function pageSMS (){
    
    pageTwoLinerText ("Enter Phone Number", "Enter Message" , typeSMS)
}
function pageTwoLinerText (inputText1, inputText2, type){
    
    var formdiv = createForm ()
    
    var form = document.createElement("form");
    form.id = "form"

    var pagecontainer = createPageContainerDiv()
    
    var inputNumber = createInputTextElement ("inputNumber", "qrstring", "qrstring", inputText1, MAX_TEXT , MAX_WIDTH)
    
    var inputMessage = createInputTextElement ("inputMessage", "qrstring", "qrstring", inputText2, MAX_TEXT , MAX_WIDTH)
    

//    var answerdiv = document.createElement("div")
//    var answer = document.createElement ("p")
//    answer.id = "answer"
//    answerdiv.appendChild (answer)
    var answerdiv = createAnswerDiv()
    
    form.appendChild (inputNumber)
    form.appendChild(nextLine())
    form.appendChild (inputMessage)
    form.appendChild(nextLine())
    
    if (type==typeWIFI){
        var encryptionSelect = encryptionType()
        form.appendChild (encryptionSelect)
        encryptionSelect.addEventListener("change" , submitQRSMSForm)
    }
    
    var imageQR = createImageQR ()

    form.appendChild (answerdiv)
    formdiv.appendChild (form)    
    pagecontainer.appendChild(formdiv)
    
    document.body.append (nextLine())
    document.body.append (imageQR)
    setimage("test")
    
    inputNumber.addEventListener("input" , submitQRSMSForm)
    inputMessage.addEventListener("input" , submitQRSMSForm)
    
    
}

function pageAddress (){
    
    //alert ("pageAddress ")
    //removePage()
    
//    var formdiv = document.createElement("div")
//    formdiv.id ="formid"
//    formdiv.setAttribute("style" ,"width:800px; margin:0 auto;")
    
    var formdiv = createForm ()
    
    var form = document.createElement("form");
    form.id = "form"

    var pagecontainer = createPageContainerDiv()
    
    
    var inputName = createInputTextElement ("inputName", "qrstring", "qrstring", "Name", MAX_TEXT , MAX_WIDTH)
    
    var inputTel = createInputTextElement ("inputTel", "qrstring", "qrstring", "Telephone Number", MAX_TEXT , MAX_WIDTH)
    
    var inputAddress = createInputTextElement ("inputAddress", "qrstring", "qrstring", "Address", MAX_TEXT , MAX_WIDTH)
    
    var inputPostalCode = createInputTextElement ("inputPostal", "qrstring", "qrstring", "Postal Code", MAX_TEXT , MAX_WIDTH)

    var inputCity = createInputTextElement ("inputCity", "qrstring", "qrstring", "City", MAX_TEXT , MAX_WIDTH)

    var inputCountry = createInputTextElement ("inputCountry", "qrstring", "qrstring", "Country", MAX_TEXT , MAX_WIDTH)
    
    var inputEmail = createInputTextElement ("inputEmail", "qrstring", "qrstring", "Email", MAX_TEXT , MAX_WIDTH)
//    var labelTextLabel = createLabelElement ("label-title" , "label-text", "Name")
    
    
    var selectCountry = createCountryList();
    
  
    
    var answerdiv = createAnswerDiv ();
    
    
    form.appendChild (inputName)
    form.appendChild(nextLine())
    form.appendChild (inputTel)
    form.appendChild(nextLine())
    form.appendChild (inputAddress)
    form.appendChild(nextLine())
    form.appendChild (inputPostalCode)
    form.appendChild(nextLine())
    form.appendChild (inputCity)
    form.appendChild(nextLine())
    //form.appendChild (inputCountry)
    form.appendChild (selectCountry)
    form.appendChild(nextLine())
    form.appendChild (inputEmail)
    form.appendChild(nextLine())
    form.appendChild (answerdiv)
    var imageQR = createImageQR ()
    
    formdiv.appendChild (form)    
    pagecontainer.appendChild(formdiv)
    
    document.body.append (nextLine())
    document.body.append (imageQR)
    setimage("test")
    
    
    inputName.addEventListener("input" , submitQRAddressForm)
    inputTel.addEventListener("input" , submitQRAddressForm)
    inputAddress.addEventListener("input" , submitQRAddressForm)
    inputPostalCode.addEventListener("input" , submitQRAddressForm)
    inputCity.addEventListener("input" , submitQRAddressForm)
    //inputCountry.addEventListener("input" , submitQRAddressForm)
    inputEmail.addEventListener("input" , submitQRAddressForm)
    selectCountry.addEventListener("input" , submitQRAddressForm)
    //document.getElementById("btnSubmit").addEventListener("click", addStudentResultButton);
    
}


function pagePhone (){
    
    pageOneLinerText ("Enter your phone number", "12345", "Your Phone Number", typePhone)
    
}

function pageURL (){
    
    
    pageOneLinerText ("Type Any URL", "https://www.google.com", "Your URL", typeURL)
}


function pageFreeText (){
    
    pageOneLinerText ("Type Any Text", "Type Your Text Here", "Your Text", typeFREE)
}

function submitQRSMSForm(){
    var data = []
    
    var tempPhone = {"name" : document.getElementById("inputNumber").value}
    var tempMessage = {"address" : document.getElementById("inputMessage").value}
    
    var encryptionElement = document.getElementById ("select-encryption")
    
    if (encryptionElement != null){
        
        
        var tempEncryptionType = {"encyrption" : encryptionElement.value}
//        console.log ("tempEncryptionType :" + tempEncryptionType)
        
        data.push ((tempPhone) , (tempMessage) , (tempEncryptionType))
    }
    else
        data.push ((tempPhone) , (tempMessage))
    

    
    setimage (JSON.stringify(data))   
    
    
}

function submitQRAddressForm(){
    
    var data = []
    
    var tempName = {"name" : document.getElementById("inputName").value}
    var tempTel = {"address" : document.getElementById("inputTel").value}
    var tempAddress = {"tel" : document.getElementById("inputAddress").value}
    var tempPostal = {"postalcode" : document.getElementById("inputPostal").value}
    var tempCity = {"city" : document.getElementById("inputCity").value}
    //var tempCountry = {"country" : document.getElementById("inputCountry").value}
    var tempEmail = {"email" : document.getElementById("inputEmail").value}
    
    var countryElement = document.getElementById ("select-country").options[document.getElementById ("select-country").selectedIndex].value
    
//    console.log ("country :" + countryElement)
        
    var tempCountry = {"country" : countryElement}
    
        

   
    data.push ((tempName) , (tempTel), (tempAddress), (tempPostal), (tempCity) , (tempCountry), (tempEmail))
    

    
    setimage (JSON.stringify(data))
}

function pageOneLinerText(labelPageText, inputPageText, labelPageAnswer, type ){
    

    
//    var formdiv = document.createElement("div")
//    formdiv.id ="formid"
//    formdiv.setAttribute("style" ,"width:800px; margin:0 auto;")
    
    var formdiv = createForm()
    
    var form = document.createElement("form");
    form.id = "form"

    
    var labelTextLabel = createLabelElement ("label-title" , "label-text", labelPageText)
    
    if (type ==typePhone){
        var inputText = createInputTextElement ("inputtext", "qrstring", "qrstring", inputPageText, MAX_PHONE_TEXT , MAX_WIDTH)
        
        
        }
    else    
        var inputText = createInputTextElement ("inputtext", "qrstring", "qrstring", inputPageText, MAX_TEXT , MAX_WIDTH)

    
    var labelAnswer = createLabelElement ("label-answer" , "label-answer", labelPageAnswer)
    
//    var answerdiv = document.createElement("div")
//    var answer = document.createElement ("p")
//    answer.id = "answer"
//    answerdiv.appendChild (answer)
    var answerdiv = createAnswerDiv ();
    
    form.appendChild (labelTextLabel)

    form.appendChild (inputText)
    form.appendChild(nextLine())
    form.appendChild(labelAnswer)
    form.appendChild(answerdiv)
    
    var imageQR = createImageQR ()
    form.appendChild(imageQR)
    var pagecontainer = createPageContainerDiv ()
    

    
    formdiv.appendChild (form) 
    


   
    pagecontainer.appendChild(formdiv)

    document.body.append (nextLine())

    document.getElementById("inputtext").addEventListener('input', function(e){
         

          var textinput = document.getElementById("answer")
            textinput.innerHTML = document.getElementById("inputtext").value

          var mydata = document.getElementById("inputtext").value;
          textinput.innerHTML = mydata

          setimage (mydata)
        

        })
    





}



function getParamURL (){
    
    var data={}
    const url = new URL(window.location.href);

    if (url.searchParams.get("type") == "free")
        data.type="free"

    else if (url.searchParams.get("type") == "url")
        
        data.type="url"
    else if (url.searchParams.get("type")== "phone")
            
        data.type="phone"
   
    else if (url.searchParams.get ("type")=="address")
        data.type = "address"
    else if (url.searchParams.get("type") == "sms")
        data.type = "sms"
    else if (url.searchParams.get("type") == "wifi")
        data.type = "wifi"
    else if (url.searchParams.get("type") == "email")
        data.type = "email"
    else {
        data.type="free"
        

    }
    
   
    return data
    
    

}

function removePage (){
    
    if ($('#page-container').length)
        $('#page-container').remove()
}
function startPage (){
    
    //removePage ()
    var data = getParamURL ()
    
    if (data.type=="free")
        pageFreeText ()
    else if (data.type=="url")
        pageURL()
    else if (data.type=="phone")
        pagePhone()
    else if (data.type=="address")
        pageAddress()
    else if (data.type=="sms")
        pageSMS ()
    else if (data.type =="wifi")
        pageWIFI()
    else if (data.type=="email")
        pageEmail()
    
    else 
        pageFreeText()
    
    setimage('hello')
    
                var footer = footerElement();
        document.body.append(footer)
}




startPage()